package kaeuchoa.desafiofull_stack.fragments

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v4.app.DialogFragment
import android.view.View
import android.widget.Button
import kaeuchoa.desafiofull_stack.R

class ConfigDialog : DialogFragment() {

    var txtInpIP: TextInputEditText? = null
    var btnConfirm: Button? = null
    var btnCancel: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val view: View = getActivity()?.getLayoutInflater()?.inflate(R.layout.config_dialog, null)!!

        this.txtInpIP = view.findViewById(R.id.inp_ip)
        this.btnConfirm = view.findViewById(R.id.btn_config_ok)
        this.btnCancel= view.findViewById(R.id.btn_config_cancel)

        val alert = AlertDialog.Builder(activity)
        alert.setView(view)

        this.btnConfirm!!.setOnClickListener {

            val IP = this.txtInpIP!!.text.toString()
            (activity as(ConfigResponse)).onResult(IP)
            dismiss()

        }

        this.btnCancel!!.setOnClickListener {
            dismiss()
        }


        return alert.create()
    }

    interface  ConfigResponse{
        fun onResult(resultIP : String) // metodo que sera chamado para retornar o resultado da operação
    }
}