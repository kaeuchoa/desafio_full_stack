package kaeuchoa.desafiofull_stack.models

class VideoGameList (val success: Boolean, val data: List<VideoGame>)