package kaeuchoa.desafiofull_stack.retrofit

import kaeuchoa.desafiofull_stack.models.Game
import kaeuchoa.desafiofull_stack.models.GameList
import kaeuchoa.desafiofull_stack.models.VideoGame
import kaeuchoa.desafiofull_stack.models.VideoGameList
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface GameService{
    @GET("game")
    fun list() : Call<GameList>

    @GET("game/console/{id}")
    fun listByConsoleId(@Path("id") id : String) : Call<GameList>

    @POST("game")
    fun insert(@Body game: Game): Call<APIResponse>
}