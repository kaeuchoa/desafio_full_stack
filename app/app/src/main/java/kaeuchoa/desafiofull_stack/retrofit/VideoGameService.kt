package kaeuchoa.desafiofull_stack.retrofit

import kaeuchoa.desafiofull_stack.models.VideoGame
import kaeuchoa.desafiofull_stack.models.VideoGameList
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface VideoGameService{
    @GET("video-game")
    fun list() : Call<VideoGameList>

    @POST("video-game")
    fun insert(@Body videoGame: VideoGame): Call<APIResponse>
}