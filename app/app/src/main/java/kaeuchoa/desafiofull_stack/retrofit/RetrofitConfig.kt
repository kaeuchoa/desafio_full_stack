package kaeuchoa.desafiofull_stack.retrofit

import android.content.Context
import kaeuchoa.desafiofull_stack.utils.PreferencesHelper
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitConfig (val context : Context){

    // Get a instance of PreferencesHelper class
    val preferencesHelper = PreferencesHelper(context)
    // save token on preferences
    val IP = preferencesHelper.serverIP

    private val retrofit = Retrofit.Builder()
//            .baseUrl("http://192.168.0.21:8080")
            .baseUrl("http://$IP:8080")
            .addConverterFactory(GsonConverterFactory.create())
            .build()


    fun videoGameService() =  retrofit.create(VideoGameService::class.java)
    fun gameService() =  retrofit.create(GameService::class.java)


}