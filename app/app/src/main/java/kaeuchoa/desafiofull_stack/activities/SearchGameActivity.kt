package kaeuchoa.desafiofull_stack.activities

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import kaeuchoa.desafiofull_stack.R
import kaeuchoa.desafiofull_stack.adapters.ListGameAdapter
import kaeuchoa.desafiofull_stack.models.GameList
import kaeuchoa.desafiofull_stack.models.VideoGameList
import kaeuchoa.desafiofull_stack.retrofit.RetrofitConfig
import kotlinx.android.synthetic.main.activity_search_game.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchGameActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_game)

        val spinnerConsole = findViewById<Spinner>(R.id.spin__search_console)
        val FABSearch = findViewById<FloatingActionButton>(R.id.fab_search_games)

        var consoleId: String = ""

        // Retrieve list of video games
        val call = RetrofitConfig(this).videoGameService().list()

        call.enqueue(object : Callback<VideoGameList> {
            override fun onFailure(call: Call<VideoGameList>?, t: Throwable?) {
                Log.e("onFailure error", t?.message)
            }

            override fun onResponse(call: Call<VideoGameList>?, response: Response<VideoGameList>?) {
                response?.body()?.let {
                    // Get names to populate spinner adapter
                    var arrayOfNames = emptyArray<String>()
                    for (videoGame in it.data){
                        arrayOfNames += videoGame.name
                    }
                    // Create and set the adapter
                    val adapter = ArrayAdapter(this@SearchGameActivity, android.R.layout.simple_spinner_item, arrayOfNames)
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    spinnerConsole.setAdapter(adapter)

                    spinnerConsole.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                        override fun onNothingSelected(p0: AdapterView<*>?) {
                            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                        }

                        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                            val videoGame = it.data.get(p2)
                            consoleId = videoGame._id
                        }
                    }
                }
            }
        })


        FABSearch.setOnClickListener{
            getGamesList(consoleId)
        }
    }

    private fun getGamesList(id : String){

        val call = RetrofitConfig(this).gameService().listByConsoleId(id)
        call.enqueue(object : Callback<GameList> {
            override fun onFailure(call: Call<GameList>?, t: Throwable?) {
                Log.e("onFailure error", t?.message)
            }

            override fun onResponse(call: Call<GameList>?, response: Response<GameList>?) {
                response?.body()?.let {
                    val response = it
                    Log.i("OnResponse", response.toString())
                    loadList(response)
                }
            }

        })
    }

    private fun loadList(gameList : GameList){
        val recyclerView = rcview_list_search_games
        val llResults = ll_results
        recyclerView.adapter = ListGameAdapter(gameList.data,this);
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
        recyclerView.layoutManager = layoutManager
        llResults.visibility = View.VISIBLE
    }
}
