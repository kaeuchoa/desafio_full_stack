package kaeuchoa.desafiofull_stack.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.Toast
import kaeuchoa.desafiofull_stack.R
import kaeuchoa.desafiofull_stack.fragments.ConfigDialog
import kaeuchoa.desafiofull_stack.utils.PreferencesHelper

class MainActivity : AppCompatActivity(), ConfigDialog.ConfigResponse {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnAddVideoGame = findViewById<Button>(R.id.btn_main_add_vg)
        val btnListVideoGame = findViewById<Button>(R.id.btn_main_list_vg)

        val btnAddGame = findViewById<Button>(R.id.btn_main_add_game)
        val btnListGame = findViewById<Button>(R.id.btn_main_list_game)
        val btnSearchGame = findViewById<Button>(R.id.btn_main_search_game)

        btnAddVideoGame.setOnClickListener{
            val intent = Intent(this, AddVideoGameActivity::class.java)
            startActivity(intent)
        }

        btnListVideoGame.setOnClickListener {
            val intent = Intent(this, ListVideoGameActivity::class.java)
            startActivity(intent)
        }

        btnAddGame.setOnClickListener {
            val intent = Intent(this, AddGameActivity::class.java)
            startActivity(intent)
        }

        btnListGame.setOnClickListener {
            val intent = Intent(this, ListGameActivity::class.java)
            startActivity(intent)
        }

        btnSearchGame.setOnClickListener {
            val intent = Intent(this, SearchGameActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId){
            R.id.menu_config->{
                openConfigDialog()
                return true
            }
        }

        return true;
    }


    fun openConfigDialog(){
        val configDialog = ConfigDialog()
        configDialog.show(supportFragmentManager,"CONFIG_DIALOG")
    }

    override fun onResult(resultIP: String) {
        // Get a instance of PreferencesHelper class
        val preferencesHelper = PreferencesHelper(this)

        // save token on preferences
        preferencesHelper.serverIP = resultIP

        Toast.makeText(this,"IP salvo com sucesso!", Toast.LENGTH_SHORT).show()
    }


}
