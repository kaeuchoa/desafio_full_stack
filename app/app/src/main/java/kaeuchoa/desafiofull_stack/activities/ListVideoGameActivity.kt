package kaeuchoa.desafiofull_stack.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import kaeuchoa.desafiofull_stack.R
import kaeuchoa.desafiofull_stack.adapters.ListVideoGameAdapter
import kaeuchoa.desafiofull_stack.models.VideoGameList
import kaeuchoa.desafiofull_stack.retrofit.RetrofitConfig
import kotlinx.android.synthetic.main.activity_list_video_game.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ListVideoGameActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_video_game)

        getVideoGamesList()


    }

    private fun getVideoGamesList(){

        val call = RetrofitConfig(this).videoGameService().list()
        call.enqueue(object : Callback<VideoGameList> {
            override fun onFailure(call: Call<VideoGameList>?, t: Throwable?) {
                Log.e("onFailure error", t?.message)
            }

            override fun onResponse(call: Call<VideoGameList>?, response: Response<VideoGameList>?) {
                response?.body()?.let {
                    val response = it
                    Log.i("OnResponse", response.toString())
                    loadList(response)
                }
            }

        })
    }

    private fun loadList(videoGameList : VideoGameList){
        val recyclerView = rcview_list_vg
        recyclerView.adapter = ListVideoGameAdapter(videoGameList.data,this);
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
        recyclerView.layoutManager = layoutManager
    }


}
