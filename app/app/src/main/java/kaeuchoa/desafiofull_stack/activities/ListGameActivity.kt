package kaeuchoa.desafiofull_stack.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import kaeuchoa.desafiofull_stack.R
import kaeuchoa.desafiofull_stack.R.id.rcview_list_game
import kaeuchoa.desafiofull_stack.adapters.ListGameAdapter
import kaeuchoa.desafiofull_stack.models.GameList
import kaeuchoa.desafiofull_stack.retrofit.RetrofitConfig
import kotlinx.android.synthetic.main.activity_list_game.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ListGameActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_game)

        getGamesList()

    }

    private fun getGamesList(){

        val call = RetrofitConfig(this).gameService().list()
        call.enqueue(object : Callback<GameList> {
            override fun onFailure(call: Call<GameList>?, t: Throwable?) {
                Log.e("onFailure error", t?.message)
            }

            override fun onResponse(call: Call<GameList>?, response: Response<GameList>?) {
                response?.body()?.let {
                    val response = it
                    Log.i("OnResponse", response.toString())
                    loadList(response)
                }
            }

        })
    }

    private fun loadList(gameList : GameList){
        val recyclerView = rcview_list_game
        recyclerView.adapter = ListGameAdapter(gameList.data,this);
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
        recyclerView.layoutManager = layoutManager
    }
}
