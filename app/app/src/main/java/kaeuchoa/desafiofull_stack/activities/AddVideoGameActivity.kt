package kaeuchoa.desafiofull_stack.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.util.Log
import android.widget.Button
import android.widget.Toast
import kaeuchoa.desafiofull_stack.R
import kaeuchoa.desafiofull_stack.models.VideoGame
import kaeuchoa.desafiofull_stack.retrofit.APIResponse
import kaeuchoa.desafiofull_stack.retrofit.RetrofitConfig
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddVideoGameActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_video_game)

        val txtInputVGName = findViewById<TextInputEditText>(R.id.inp_video_game_name)
        val txtInputVGCompany = findViewById<TextInputEditText>(R.id.inp_video_game_company)

        val btnConfirm = findViewById<Button>(R.id.btn_add_vg_ok)
        val btnCancel = findViewById<Button>(R.id.btn_add_vg_cancel)

        btnConfirm.setOnClickListener {
            val vgName = txtInputVGName.text.toString()
            val vgCompany = txtInputVGCompany.text.toString()
            val call = RetrofitConfig(this).videoGameService().insert(VideoGame(vgName, vgCompany, ""))
            call.enqueue(object : Callback<APIResponse> {
                override fun onFailure(call: Call<APIResponse>?, t: Throwable?) {
                    Log.e("onFailure error", t?.message)
                }

                override fun onResponse(call: Call<APIResponse>?, response: Response<APIResponse>?) {
                    response?.body()?.let{
                        Toast.makeText(this@AddVideoGameActivity, it.message,Toast.LENGTH_SHORT).show()
                        finish()
                    }
                }
            })

        }

        btnCancel.setOnClickListener {
            onBackPressed()
            finish()
        }


    }
}
