package kaeuchoa.desafiofull_stack.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.util.Log
import android.view.View
import android.widget.*
import kaeuchoa.desafiofull_stack.R
import kaeuchoa.desafiofull_stack.models.Game
import kaeuchoa.desafiofull_stack.models.VideoGameList
import kaeuchoa.desafiofull_stack.retrofit.APIResponse
import kaeuchoa.desafiofull_stack.retrofit.RetrofitConfig
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddGameActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_game)

        val spinnerConsole = findViewById<Spinner>(R.id.spin_console)
        val btnConfirm = findViewById<Button>(R.id.btn_add_game_ok)
        val btnCancel = findViewById<Button>(R.id.btn_add_game_cancel)
        val txtInputName = findViewById<TextInputEditText>(R.id.inp_game_name)

        var consoleName = ""
        var consoleId = ""

        // Retrieve list of video games
        val call = RetrofitConfig(this).videoGameService().list()

        call.enqueue(object : Callback<VideoGameList> {
            override fun onFailure(call: Call<VideoGameList>?, t: Throwable?) {
                Log.e("onFailure error", t?.message)
            }

            override fun onResponse(call: Call<VideoGameList>?, response: Response<VideoGameList>?) {
                response?.body()?.let {
                    // Get names to populate spinner adapter
                    var arrayOfNames = emptyArray<String>()
                    for (videoGame in it.data){
                        arrayOfNames += videoGame.name
                    }
                    // Create and set the adapter
                    val adapter = ArrayAdapter(this@AddGameActivity, android.R.layout.simple_spinner_item, arrayOfNames)
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    spinnerConsole.setAdapter(adapter)

                    spinnerConsole.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                        override fun onNothingSelected(p0: AdapterView<*>?) {
                            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                        }

                        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                            val videoGame = it.data.get(p2)
                            consoleName = videoGame.name
                            consoleId = videoGame._id
                        }
                    }
                }
            }
        })



        btnConfirm.setOnClickListener {
            val name = txtInputName.text.toString()
            val call = RetrofitConfig(this).gameService().insert(Game(name,consoleName,consoleId))
            call.enqueue(object : Callback<APIResponse> {
                override fun onFailure(call: Call<APIResponse>?, t: Throwable?) {
                    Log.e("onFailure error", t?.message)
                }

                override fun onResponse(call: Call<APIResponse>?, response: Response<APIResponse>?) {
                    response?.body()?.let{
                        Toast.makeText(this@AddGameActivity, it.message,Toast.LENGTH_SHORT).show()
                        finish()
                    }
                }
            })

        }

        btnCancel.setOnClickListener {
            onBackPressed()
            finish()
        }

    }


}
