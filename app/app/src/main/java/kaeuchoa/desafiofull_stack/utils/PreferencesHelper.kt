package kaeuchoa.desafiofull_stack.utils

import android.content.Context
import android.preference.PreferenceManager

class PreferencesHelper(private val context: Context) {
    companion object {
        private val SERVER_IP = "data.source.prefs.SERVER_IP"
    }

    private val preferences = PreferenceManager.getDefaultSharedPreferences(context)

    var serverIP = preferences.getString(SERVER_IP, "")
        set(value) = preferences.edit().putString(SERVER_IP, value).apply()
}