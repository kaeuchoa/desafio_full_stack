package kaeuchoa.desafiofull_stack.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kaeuchoa.desafiofull_stack.R
import kaeuchoa.desafiofull_stack.models.Game
import kotlinx.android.synthetic.main.game_item.view.*


class ListGameAdapter(private val games : List<Game>, private val context: Context) : RecyclerView.Adapter<ListGameAdapter.ViewHolder>(){
    
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val game = games[position]
        holder.bindView(game)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.game_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return games.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(game: Game) {
            val name = itemView.txt_game_item_name
            val consoleName = itemView.txt_game_item_console_name
            val labelConsole = itemView.txt_game_label_console_name

            name.text = game.name
            consoleName.text = game.console_name
            labelConsole.text = "Console:"
        }
    }
}