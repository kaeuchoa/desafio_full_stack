package kaeuchoa.desafiofull_stack.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kaeuchoa.desafiofull_stack.R
import kaeuchoa.desafiofull_stack.models.VideoGame
import kotlinx.android.synthetic.main.video_game_item.view.*


class ListVideoGameAdapter(private val videoGames : List<VideoGame>,private val context: Context) : RecyclerView.Adapter<ListVideoGameAdapter.ViewHolder>(){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val videoGame = videoGames[position]
        holder.bindView(videoGame)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.video_game_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return videoGames.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(videoGame : VideoGame){
            val name = itemView.txt_vg_item_name
            val company = itemView.txt_vg_item_company
            name.text = videoGame.name
            company.text = videoGame.company
        }

    }
}