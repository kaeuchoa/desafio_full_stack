Desafio Full-Stack

Aplicação teste para kit full-stack.

Utiliza Node.js + Express + Mongo DB para o servidor e Android (Kotlin) com retrofit2
para o front-end (app).

Para executar o servidor:
```sh
git clone https://gitlab.com/kaeuchoa/desafio_full_stack.git
cd servidor
npm install
node server.js
```

Para executar o aplicativo. Utilizar o apk ou compilar via AndroidStudio.

Guia para instalar apk:
https://www.wikihow.tech/Install-APK-Files-on-Android
