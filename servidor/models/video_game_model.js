const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let videoGameSchema = new Schema({
  name: {type: String, required: true, max: 50},
  company : {type: String, required: true, max: 50}
});

module.exports = mongoose.model('VideoGame', videoGameSchema);
