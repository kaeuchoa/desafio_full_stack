const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// id:	“5b0aod1400aab2sctr21”,
// name:	“The	Legend	of	Zelda:	Majoras	Mask”,
// console_name:	“Nintendo	64”,
// console_id:	“5b9bf990db09acs1bca22a”

let gameSchema = new Schema({
  name: {type: String, required: true, max: 150},
  console_name : {type: String, required: true, max: 50},
  console_id: {type: String, required: true}
});

module.exports = mongoose.model('game', gameSchema);
