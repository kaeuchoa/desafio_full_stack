//ZBQgYVVkL5OA1R6B
//

const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();
const mongoose = require('mongoose');

const port = 8080;

const app = express();

// importa as rotas
const index = require('./routes/index');
const videoGameRoute = require('./routes/video_game_route');
const gameRoute = require('./routes/game_route');

app.listen(port, function () {
  console.log('Servidor iniciado em localhost: ' + port);
});

// Conexão com bd

const DB_URL = 'mongodb+srv://kae:ZBQgYVVkL5OA1R6B@clusterteste-bca09.mongodb.net/desafio_full_stack?retryWrites=true';

mongoose.connect(DB_URL, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use('/', index);
app.use('/video-game',videoGameRoute);
app.use('/game',gameRoute);

module.exports = app;
