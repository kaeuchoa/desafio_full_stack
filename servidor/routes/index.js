const express = require('express');
const router = express.Router();
const TAG = 'index/';

router.get('/', function (req, res, next) {
    res.status(200).send(TAG + "Status: 200 OK ");
});

router.get('/easter-egg', function (req, res, next) {
    res.status(200).send("Me contratem pf :)");
});

module.exports = router;
