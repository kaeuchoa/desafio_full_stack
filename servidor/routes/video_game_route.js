const TAG = 'video_game_route/';
const express = require('express');
const router = express.Router();
const controller = require('../controllers/video_game_controller');

router.get('/', controller.get);
router.get('/:id', controller.findById);
router.post('/', controller.post);
router.put('/:id', controller.put);
router.delete('/:id', controller.delete);
module.exports = router;
