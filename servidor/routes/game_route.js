const TAG = 'game_route/';
const express = require('express');
const router = express.Router();
const controller = require('../controllers/game_controller')


router.get('/', controller.get);
router.get('/console/:id', controller.findByConsoleId);

router.post('/', controller.post);
router.put('/:id', controller.put);
router.delete('/:id', controller.delete);

module.exports = router;
