const TAG = "video_game_controller/"
let VideoGame = require('../models/video_game_model');

exports.get = (req, res, next) => {
  console.log("GET/VideoGame");
  VideoGame.find({}, function (err, videoGames) {
    let dados = {success: true, data: []};
    videoGames.forEach(function (value,index) {
      dados.data.push(value);
    })
    res.send(dados);

    // res.send(videoGames)
  });
};

exports.findById = (req,res,next) => {
  console.log("FINDBYID/VideoGame");
  VideoGame.findById(req.params.id, function (err, videoGame) {
       if (err) return next(err);
       res.send(videoGame);
   })
}

exports.post = (req, res, next) => {
  console.log("POST/VideoGame");
    console.log(req.body);
    let newVideoGame = new VideoGame({
      name: req.body.name,
      company : req.body.company
    });

    newVideoGame.save(function (err) {
       if (err) {
           return next(err);
       }
       res.status(201).send({status: true, message: 'Video-game criado com sucesso' });
   });
};


exports.put = (req, res, next) => {
    let id = req.params.id;
    res.status(201).send(TAG + `PUT/Funcionando! ID inserida : ${id}`);
};

exports.delete = (req, res, next) => {
    let id = req.params.id;
    res.status(200).send(TAG + `DELETE/ Funcionando! ID inserida :${id}`);
};
