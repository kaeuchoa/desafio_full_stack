const TAG = "game_controller/";
let Game = require('../models/game_model');

//name: {type: String, required: true, max: 150},
// console_name : {type: String, required: true, max: 50},
// console_id: {type: Number, required: true}
exports.get = (req, res, next) => {
  console.log("GET/Game");
  Game.find({}, function (err, games) {
    let dados = {success: true, data: []};
    games.forEach(function (value,index) {
      dados.data.push(value);
    })
    res.send(dados);
  });
};

exports.findByConsoleId =  (req, res, next) => {
  console.log("FINDBYCONSOLEID/Game");
  Game.find({console_id: req.params.id}, function (err, games) {
    let dados = {success: true, data: []};
    games.forEach(function (value,index) {
      dados.data.push(value);
    })
    res.send(dados);
  });
}

exports.post = (req, res, next) => {
  console.log("POST/Game");
  console.log(req.body);
  let newGame = new Game({
    name: req.body.name,
    console_name: req.body.console_name,
    console_id: req.body.console_id
  });

  newGame.save(function (err) {
    if (err) {
      return next(err);
    }
    res.status(201).send({status: true, message: "Jogo inserido com sucesso!"});
  });
};


exports.put = (req, res, next) => {
  Game.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, game) {
    if (err) return next(err);
    res.status(201).send('Game Atualizado.');
  });
};

exports.delete = (req, res, next) => {
  Game.findByIdAndRemove(req.params.id, function (err) {
    if (err) return next(err);
    res.status(201).send('Game apagado com sucesso!');
  })
};
